#lang racket

(require racket/syntax)
(require "structs.rkt")
(require "families.rkt")
(require "split-funs.rkt")

(define (make-name/xmg tree)
  (define (node->string n)
    (string-append (string-replace (Node-cat n) "_" "")
		   "-"
		   (symbol->string
		    (match (Node-color n)
		      ['black 'b]
		      ['white 'w]
		      ['white-r 'w]))))
  (define (p-h tree trail)
    (let* ([tn (car (top-nodes tree))]
	   [dtrs (get-node-daughters tree tn)])
      (cond
       [(null? dtrs)
	(list (string-append trail (node->string tn)))]
       [else
	(cons (string-append trail (node->string tn))
	      (flatten
	       (map (curryr p-h (string-append trail "_"))
		    (map (curry get-subtree tree)
			 (sort/nodes dtrs (Tree-orderings tree))))))])))
  (apply string-append (p-h tree "")))

(define (clean-tree tree)
  (Tree (make-name/xmg tree)
	(map (lambda (y) (if (equal? (Node-color y) 'white-r)
			(Node (Node-id y)
			      (Node-cat y)
			      'white
			      (Node-features y))
			y))
	     (Tree-nodes tree))
	(Tree-edges tree)
	(Tree-orderings tree)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (remove-duplicates/fragments trs-list [seen '()] [seen-names '()])
  (cond
   [(null? trs-list) seen]
   [(member? (Tree-name (car trs-list)) seen-names)
    (remove-duplicates/fragments (cdr trs-list) seen seen-names)]
   [else
    (remove-duplicates/fragments (cdr trs-list)
				 (cons (car trs-list) seen)
				 (cons (Tree-name (car trs-list)) seen-names))]))


(define trees
  (map paint-all-black
       (read (open-input-file "./data/xtag-trees.parse"))))

(define trees-dict
  (for/hash ([tr trees])
    (values (Tree-name tr) tr)))

(define splitted-trees 
  (read (open-input-file "./data/splitted-trees-5.parse")))

(define all-fragments
  (remove-duplicates/fragments
   (map clean-tree
	(flatten (hash-values splitted-trees)))))

(define all-fragments-1
  (remove-duplicates/trees
   (map clean-tree
	(flatten (hash-values splitted-trees)))))


(length all-fragments)
(length all-fragments-1)
(length trees)

(define fragments-dict
  (let ([frgs all-fragments])
    (for/hash ([frag frgs])
      (values (make-name/xmg frag) frag))))

(map pprint (dict-ref splitted-trees "betaNpxnx1Vpbynx0"))

( pprint (dict-ref trees-dict "betaNpxnx1Vpbynx0"))

(map Tree-name (dict-ref splitted-trees "betaNpxnx1Vpbynx0") )

(map make-name/xmg (dict-ref splitted-trees "betaNpxnx1Vpbynx0"))

(map Tree-name (dict-ref splitted-trees "betaNc1nx0PNaPnx1"))

(map make-name/xmg (dict-ref splitted-trees "betaNc1nx0PNaPnx1"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (replicate n str)
  (apply string-append (make-list str n)))

(define (fragment->xmg tree)
  (define (mk-head)
    (apply string-append
	   (flatten
	    (list
	     "class "
	     (make-name/xmg tree)
	     ;; "\ndeclare\n"
	     ;; (map (curry string-append "?")
	     ;; 	  (map (curryr string-append " ")
	     ;; 	       (map Node-cat (Tree-nodes tree))))
	     "\n{<syn>\n"))))
  (define (node->xmg n)
    (string-append "node "
		   ;; (Node-cat n)
		   "("
		   "color = " (mk-color n)
		   ", mark = " (mk-mark n)
		   ")"
		   "["
		   "cat = " (mk-cat n)
		   ", bot = [], top = []"
		   "]"))
  (define (mk-mark n)
    (car (regexp-match "[^_]*?$"  (Node-cat n))))
  (define (mk-color n)
    (string-replace (symbol->string (Node-color n)) "white-r" "white"))
  ;; (define (mk-cat n)
  ;; (string-downcase (regexp-replace "_.*" (Node-cat n) "")))
  (define (mk-cat n)
    (string-downcase (regexp-replace "_[^_]*?$" (Node-cat n) "")))
  (define (p-h tree trail)
    (let* ([tn (car (top-nodes tree))]
	   [dtrs (get-node-daughters tree tn)])
      (cond
       [(null? dtrs)
	(string-append trail (node->xmg tn) "\n")]
       [else
	(string-append
	 (string-append trail (node->xmg tn) "\n")
	 "{\n"
	 (apply string-append
		(flatten
		 (map (curryr p-h (string-append trail "  "))
		      (map (curry get-subtree tree)
			   (sort/nodes dtrs (Tree-orderings tree))))))
	 "}\n")])))
  (string-append (mk-head) "{" (p-h tree "") "}}"))

(define (tree->xmg tr-name splits-dict)
  (apply
   string-append
   (flatten
    (list "class "
	  tr-name
	  "\nimport\n"
	  (map (curryr string-append "[]\n")
	       (map make-name/xmg
		    (dict-ref splits-dict tr-name)))))))

;; write to file

(display
 (remove-duplicates
  (flatten
   (map (lambda (tr)
	  (map mk-cat (Tree-nodes tr)))
	all-fragments))))

(call-with-output-file
    "./xmg/fragments.mg"
  (lambda (out)
    (for ([frg all-fragments])
      (displayln (fragment->xmg frg) out)
      (displayln "\n\n" out)))
  #:exists 'truncate)

(call-with-output-file
    "./xmg/classes.mg"
  (lambda (out)
    (for ([tr-name (dict-keys splitted-trees)])
      (displayln (tree->xmg tr-name splitted-trees)
		 out)))
  #:exists 'truncate)


