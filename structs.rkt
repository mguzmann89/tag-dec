#lang racket

(provide (all-defined-out))

(struct Node 
  (id cat color features) #:prefab)

(struct Edge 
  (mother daughter) #:prefab)

(struct Ordering 
  (first second) #:prefab)

(struct Tree 
  (name nodes edges orderings) #:prefab)

(struct Fts (coref name value) #:prefab)

;; basic printing
(define (ordering->string ord)
  (string-append
   (number->string (Ordering-first ord))
   " > "
   (number->string (Ordering-second ord))))

(define (edge->string edge)
  (string-append
   (number->string (Edge-mother edge))
   " -> "
   (number->string (Edge-daughter edge))))

(define (node->string node)
  (string-append
   (number->string (Node-id node))
   "-" (Node-cat node)
   "-" (symbol->string (Node-color node))))

(define (obj->string obj)
  (cond
   [(Ordering? obj) (ordering->string obj)]
   [(Edge? obj) (edge->string obj)]
   [(Node? obj) (node->string obj)]))


